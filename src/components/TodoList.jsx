import React, { useEffect, useState } from "react";
import NewTodoForm from "./NewTodoForm";
import TodoListItem from "./TodoListItem";
import "../assets/styles/TodoList.css";
import axios from "axios";

const TodoList = () => {
  const [todos, setTodos] = useState([]);

  useEffect(() => {
    // axios
    axios
      .get("https://jsonplaceholder.typicode.com/posts")
      .then((res) => {
        setTodos(res.data);
      }) // handle response jika sukses
      .catch((e) => console.log("error", { e })) // handle response jika gagal
      .then(() => console.log("request selesai")); // handle ketika request selesai
  }, []);

  const addNewTodo = (item) => {
    // handle perubahan data baru
    setTodos([item, ...todos]);

    /**
     * Spread operator (...variable) = nge-copy data item suatu array atau object
     */
    // console.log("spread todos", ...todos);
    // console.log("new item", item);
    // console.log("new data", [...todos, item]);
  };

  const deleteTodo = (id) => {
    // request delete
    axios
      .delete(`https://jsonplaceholder.typicode.com/posts/${id}`)
      .then((res) => {
        console.log(res);
        // filter new data
        const newData = todos.filter((item) => item.id !== id);
        setTodos(newData);
      })
      .catch((e) => console.log({ e }));
  };

  return (
    <div className="list-wrapper">
      <NewTodoForm data={todos} addTodo={addNewTodo} />
      {todos.map((todo, index) => (
        <TodoListItem todo={todo} key={index} delTodo={deleteTodo} />
      ))}
    </div>
  );
};

export default TodoList;
