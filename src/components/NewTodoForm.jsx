import axios from "axios";
import React, { useState } from "react";
import "../assets/styles/NewTodoForm.css";

const NewTodoForm = ({ data, addTodo }) => {
  const [inputValue, setInputValue] = useState("");

  const inputTodo = () => {
    console.log("value", inputValue);
    const newData = {
      userId: 2,
      id: data.length + 1,
      title: inputValue,
      // body: "Body default",
    };

    axios
      .post("https://jsonplaceholder.typicode.com/posts", newData)
      .then((res) => {
        console.log(res);
        addTodo(res.data);
      })
      .catch((e) => console.log({ e }))
      .then(() => setInputValue(""));

    // try {
    //   const res = await axios.post(
    //     "https://jsonplaceholder.typicode.com/posts",
    //     newData
    //   );
    //   addTodo(res.data);
    // } catch (error) {
    //   console.log(error);
    // }
  };

  return (
    <div className="new-todo-form">
      <input
        className="new-todo-input"
        type="text"
        placeholder="Type your new todo here"
        value={inputValue}
        onChange={(e) => setInputValue(e.target.value)}
      />
      <button
        disabled={!inputValue}
        className="new-todo-button"
        onClick={inputTodo}
      >
        Create Todo
      </button>
    </div>
  );
};

export default NewTodoForm;
