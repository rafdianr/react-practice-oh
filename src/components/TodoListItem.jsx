import React from "react";
import "../assets/styles/TodoListItem.css";

const TodoListItem = ({ todo, delTodo }) => (
  <div className="todo-item__container">
    <h3 className="todo-item__title">{todo.title}</h3>
    <div className="todo-item__bottom">
      <div className="todo-item__id">ID: {todo.id}</div>
      <div className="todo-item__buttons">
        <button className="update">Update</button>
        <button className="remove" onClick={() => delTodo(todo.id)}>
          Remove
        </button>
      </div>
    </div>
  </div>
);

export default TodoListItem;
